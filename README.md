# W A Z U R E P O
This repository is intended to contain all my libraries and what-not in a way
in which nimble can use.

I'm doing my own repository because I don't really... want to get involved
with others due to severe differences of opinions regarding development, and
to top that off I refuse to use `github` so I figure this is the best course
of action for myself.

It isn't that I have anything against the nimble developers or the `Nim`
community at all - I just want to do things this way. If the `nimble` folks want
to add my repository to their own that is entirely up to them I just cannot
guarantee that any or all of my libraries will remain up-to-date with latest
`Nim` or not get shuffled around a bit.

This is first and foremost for my own benefit - users be warned.

## Usage
[This][1] should explain what you need to know provided they don't change
the docs.

[1]:https://github.com/nim-lang/nimble#configuration

